# Länkar

## WikiData

På initiativet Wikidata ([https://www.wikidata.org](https://www.wikidata.org)) görs ett gediget arbete för att kunna länka och bygga kunskap utefter data. Genom att göra den här specifikationen kompatibel med Wikidata genom länkning hoppas vi kunna bidra till att höja kvaliteten i Wikidata och i specifikationen. Vill du att dina bad i kommunen syns bättre i dessa plattformar - se till att bilder på baden finns tillgängliga med med fri licens som CC-0 och/eller ladda upp bilder på [WikiCommons](https://commons.wikimedia.org/wiki/Main_Page).

Använd fältet “wikidata”, egenskap nummer 6 i denna specifikation, för att länka till den det objekt i Wikidata som beskriver badplatsen. Det kommer att vara ett ID i Wikidata för varje badplats.

Wikidata-id anges med en så kallad “Q-kod” och börjar med bokstaven Q och följs sedan av ett antal siffror. Detta är det unika ID som badplatsen har i Wikidatas databas. Om det inte finns en Q-kod för ditt bad, rekommenderas du att lägga till badplatsen på Wikidata och länka dit.

Om badplatsen finns med i Wikidata öppnas fler möjligheter och rekommendationen är att du skapar ett objekt där som beskriver din badplats. Här är ett exempel på hur man kan använda Wikidata för att t.ex. rita en karta och länka till andra datakällor på Wikidata gällande Svenska badplatser: [https://w.wiki/3MZg](https://w.wiki/3MZg) 

## Open Street Map

Under arbetet att ta fram denna specifikation, fördes samtal med flera personer och grupper insatta i Open Street Map. Vi önskade att specifikationen för badplatser blir kompatibel med och kan användas av initiativet på bästa sätt.

Vi beslöt att stryka direkt referens till OSM eftersom detta initiativ saknar ett beständigt referenssystem för en plats - dessa ändras och kan redigeras bort av vem som helst. En punkt kan bli en yta som kan bli något annat, och vid varje sådan ändring förändras IDt. I dialog beslöts istället att använda wikidata -referensen i egenskap nummer 6. En referens till Wikidata ger då, när sådan finns på Wikidata, referens till OSMs aktuella kartobjekt beskrivande platsen.

## Tillgänglighetsdatabasen

Tillgänglighetsdatabasen saknar persistenta identifierare som är stabila över tid och det går inte att hänvisa till ett objekt på ett stabilt sätt annat än med hjälp av URL direkt till objektet som beskrivs. Tips har lämnats till TD om att en sådan identifierare kanske kunde skapas och göras synlig/tydlig i API och på webbplatsen, så att länkning av data kan ske på ett bra sätt.

TD har ett API som du kan nå på [Tillgänglighetsdatabasen - developer portal (azure-api.net)](https://td.portal.azure-api.net/) och som är kostnadsfritt. För att använda API:erna måste du dock registrera dig och hämta ut en personlig nyckel. APIt och dess utveckling sköts helt av Tillgänglighetsdatabasen och frågor om dess kapabilitet och funktion hänvisar vi till TD. Lämna gärna förbättringsförslag till TD som har indikerat till oss att de är intresserade av att deras tjänster nyttjas och utvecklas

### Min badplats saknas i Tillgänglighetsdatabasen

Kontakta din kommun där badplatsen ligger och be dem lägga till badplatsen. Generellt saknas många “naturstränder” men många badhus finns med.
