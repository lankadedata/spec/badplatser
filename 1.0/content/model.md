# Datamodell

Datamodellen är tabulär, där varje rad motsvarar exakt en badplats och varje kolumn motsvarar en egenskap för denna plats. 42 attribut är definierade, där de första 4 är obligatoriska. För att förenkla länkning av data och separation av avsändare av datat, är kolumnen “place_id” alltid obligatorisk. Detta ID måste vara unikt och beständigt över tid och får inte återanvändas om en badplats ex. läggs ned. Se sektion 2.1 för förtydliganden.

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Orsaken till detta är att man vill använda kolumnnamn snarare än ordningen för att detektera om viss data finns. Korta och enkla kolumnnamn minskar risken för att problem uppstår. Det är vanligt att man erbjuder söktjänster där frågor mot olika kolumner skrivs in i webbadresser och det är bra om det kan göras utan speciella teckenkodningar.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.
</div>

<div class="note" title="2">
Kolumn 3 och 4, latitude och longitude, anges på “WGS84-format” (se kapitel 2.1). Detta är det breda vedertagna koordinatsystem som används av många kommersiellt tillgängliga kart- och  navigationsverktyg. Det är enkelt att genom flera av dessa verktyg att ange eller plocka ut koordinater för en plats i detta format, varför vi har valt det som attribut för position.

För dataproducenten kan rekommenderas <a href="https://www.openstreetmap.org/">OpenStreetMap</a> för att t.ex. hitta koordinater för en viss plats genom att placera en kartnål och kopiera kartnålens koordinat.

Den som kan använder koordinater från kommunens GIS-system - tala med din GIS/Kart- och mätenhet och tillse att du om möjligt använder samma koordinater i din datamängd som finns i kommunens övriga system
</div>

<div class="ms_datatable">

| #  | Kolumnnamn      | Datatyp                         | Exempel och förklaring |
| -- | --------------- | ------------------------------- | ---------------------- |
| 1  | place\_id       | text                         | **Obligatoriskt** - Ange en unik och stabil identifierare för badplatsen inom er kommun. Det viktiga är att identifieraren är unik (tänk unik i hela världen), och att den är stabil över tid - dvs att den inte ändras mellan uppdateringar. Om en badplats avvecklas ska identifieraren inte återanvändas på en annan badplats. Läs mer om detta attribut i sektion 2.2 |
| 2  | name            | text                            | **Obligatoriskt** - Badplatsens namn, exempel för en badplats i Mariestads kommun “Hattareviksbadet” eller för badplats i Stockholms kommun “Flatenbadet, allmänna badplatsen” resp. “Flatenbadet, barnbadet” <br><br>|
| 3  | latitude        | decimaltal                      | **Obligatoriskt** - Latitud anges per format som WGS84-specifikation. WGS84 är den standard som det Amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En latitudangivelse som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. <br><br>|
| 4  | longitude       | decimaltal                      | **Obligatoriskt** - Longitud anges per format som WGS84. Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges. <br><br>|
| 5  | hov\_ref        | text                            | Värde som refererar till den unika identifierare badplatsen har hos Havs- och vattenmyndigheten (HoV). Hos HoV finns många av Sveriges kommuners badplatser med mätdata för bland annat vattenkvalitet. Genom den så kallade NUTSKOD som skapas av myndigheten, kan du koppla samman din badplats med samma badplats i datamängden hos HoV. Se förtydligande kapitel 3 i detta dokument.<br>Exempel på en NUTSKOD som beskriver badplatsen “Hjärtasjöbadet” i Osby kommun: SE0441273000000003<br><br>Fältet skall alltid anges utan mellanslag eller andra skiljetecken.<br><br>Det är det som HoV kallar för “NUTSKOD” som ska skrivas in på detta attribut “hov\_ref”. |
| 6  | wikidata        | text                            | Referens till wikidata-artikel/entry om platsen. Se kapitel “3. Wikidata" i detta dokument. Använd den sk. Q-koden till entiteten på Wikidata.<br><br>Exempel för att länka till Hattareviks badplats i Mariestads kommun på Wikidata:<br>Q106636433<br><br>Q-kod anges utan mellanslag, bindestreck eller skiljetecken och består alltid av bokstaven Q följt av ett antal siffror. <br><br>|
| 7  | updated         | datum                           | Ett datum som anger när informationen om badplatsen senast uppdaterades. Detta attribut ska anges när någon del av informationen har uppdaterats, hur liten och till synes obetydlig uppdateringen är. Utan datum blir det svårt för en implementatör att veta hur aktuell datat är och ifall det går att lita på. Datum ska anges på format enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html) utan undantag.<br><br>Exempel, onsdagen den 21:e juli 2021 ska anges enligt:<br>2021-07-21<br><br>Var noga med att uppdatera detta fält när du gör en ändring, hur liten den än är, på en badplats. <br><br>|
| 8  | description     | text                            | Kort beskrivning av platsen och omgivningen. Undvik att beskriva sådant som redan framgår i specifikationen, t.ex. behöver du inte skriva att badplatsen har sandstrand då det framgår genom att sätta J i attribut nr 31 “beach\_sand”.<br><br>Exempel på beskrivning: “Liten och avsides badstrand i Åkberga naturreservat. Stranden cirka 50 meter lång, nås till fots från Borbergets utsiktsplats. Mycket långgrund.”.<br><br>Vill du dela data på flera olika språk? Se sektion 5 och 7 för att lära dig mer om kraven på metadata i samband med publicering. <br><br>|
| 9  | street          | text                            | Gatuadress till platsen, exempelvis “Havsvägen”. Ange inte nummer på gata eller fastighet, det anges i attributet “housenumber”. Mappar mot Open Street Maps “addr:street” läs mer på [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr) <br><br>|
| 10 | housenumber     | text                            | Gatunummer eller husnummer med bokstav. Exempelvis “1” eller “42 E”. Mappar mot Open Street Maps “addr:housenumber” läs mer på: [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr) <br><br>|
| 11 | postcode        | text                            | Postnummer, exempelvis “54191”, mappar mot Open Street Maps “addr:postcode”, läs mer på: [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr) <br><br>|
| 12 | city            | text                            | Postort, exempelvis “Töreboda”. Mappar mot Open Street Maps “addr:city”, läs mer på: [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr) <br><br>|
| 13 | country         | text                            | Land där badplatsen finns. Skall anges enligt [ISO3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2). För Sverige ange “SE”. Fältet mappar mot Open Streetmaps “addr:country”. <br><br>|
| 14 | facilities      | text                            | Fritext som beskriver platsen utöver det som framkommer i listan över fasta installationer. Om toalett finns på platsen anger du detta i attributet “toilets” och du behöver inte beskriva det på nytt här. Facilities använder du när du behöver beskriva något ytterligare som du inte tycker framkommer i de övriga attributen. Exempel på saker du kan beskriva: Kiosk/försäljning, anslagstavla, förvaringsskåp, lekplats osv. <br><br>|
| 15 | toilet          | boolean                    | Beskriver om toalett av någon typ finns på platsen - spoltoalett, torrdass, kemtoa etc. |
| 16 | shower          | boolean                    | Beskriver om dusch finns på platsen.|
| 17 | drinking\_water | boolean                    | Beskriver om det finns dricksvatten på platsen. Attributet beskriver förekomst av, inte på vilket sätt (slang, kran, pump, hink, källa osv).|
| 18 | changing\_room  | boolean                    | Beskriver om omklädningsrum finns på platsen.|
| 19 | lifeguard       | boolean                    | Beskriver om badvakt/livräddare finns på platsen.|
| 20 | lifebuoy        | boolean                    | Beskriver om livboj finns på platsen.|
| 21 | trash           | boolean                    | Beskriver om soptunna finns på platsen.|
| 22 | firstaid        | boolean                    | Beskriver om första-hjälpen-utrustning finns på platsen.|
| 23 | fireplace       | boolean                    | Beskriver om särskilt anordnad eldstad finns. Attributet beskriver bara att det finns, inte hur den ser ut eller vilka möjligheter som finns (grillgaller, cementrör, murad spis/grill osv).|
| 24 | bathing\_jetty  | boolean                    | Beskriver om badbrygga finns på platsen.|
| 25 | bathing\_ladder | boolean                    | Beskriver om badstege finns på platsen.|
| 26 | diving\_tower   | boolean                    | Beskriver om hopptorn finns på platsen.|
| 27 | td\_url         | URL                             | Länk till Tillgänglighetsdatabasen, TD. Det finns inga unika IDn att länka till utan du länkar till hela det URL som går direkt till ett objekt/plats som är upplagd i Tillgänglighetsdatabasen. Se sektion NNN i detta dokument för mer information om Tillgänglighetsdatabasen.|
| 28 | accessibility   | text                            | Beskriver platsens tillgänglighet utöver det som framkommer i den fasta listan attribut. Här kan du ex.beskriva hur och med vad platsen anpassats för olika funktionsnedsättningar, eller dess tillgänglighet i form av öppettider, restriktioner, säsong osv. Fokusera helt på platsens tillgänglighet i detta attribut.|
| 29 | wheelchair      | boolean                    | Anger om badplatsen är helt anpassad för att erbjuda bad för en rullstolsburen person. Detta innebär att parkering, tillgång till stranden, tillgång till vattnet via ramp/hiss etc., är anpassat - kort sagt hela badplatsen är tillgänglig för rullstolsburen person.<br><br>Du ska inte markera "true" i detta attribut om inte att alla delar av badplatsen är anpassad. Att det t.ex. finns en parkeringsplats för rullstolsburen betyder alltså inte att du kan sätta "true" i detta attribut. Förslagsvis använder du attributet “td\_url” för att peka ut platsens anpassningar för rullstolsburen eller rörelsehindrad.|
| 30 | watertype       | sea &vert; lake &vert; pool &vert; watercourse | Beskriver huvudsaklig tillgänglig vattentyper för bad, kan vara en av: sea = hav (Salt eller bräckt vatten). lake = insjö. pool = pool eller bassäng/klorerat, watercourse = flod, å, älv, bäck eller annat rinnande vatten eller vattendrag i rörelse.|
| 31 | beach\_sand     | boolean                    | Har badplatsen sandstrand.|
| 32 | beach\_stone    | boolean                    | Har badplatsen stenstrand (klappersten, småsten, grus, stenblock).|
| 33 | beach\_rock     | boolean                    | Har badplatsen kilppstrand (naturligt förekommande klippor eller stenhällar).|
| 34 | beach\_concrete | boolean                    | Har badplatsen betong/kakel/klinkstrand. (Används om du beskriver en utebassäng som är kaklad/klinklagd). |
| 35 | beach\_grass    | boolean                    | Har badplatsen gräs vid strand eller grässtrand.|
| 36 | pet\_bath       | boolean                    | Beskriver om husdjur (ex. hund) får bada på platsen, där t.ex. särskild del av strand är avlyst för djur.|
| 37 | camping         | boolean                    | Anger om det är tillåtet att campa på badplatsen. Om du anger “false” betyder det att det är förbjudet att campa på platsen. |
| 38 | temp\_url       | URL                             | En eller flera länk till tjänster eller information om aktuella vattentemperaturer, ex direktlänk till ett API eller till en plats/tjänst som visa aktuella temperaturer. Exempel:<br> https://iotplatform.kommun.se/badvatten<br> http://kommun.se/strandbad |
| 39 | extra\_url      | URL                             | Länk till valfri ytterligare tjänst eller information om till exempel vattenkvalitet, API eller annan relaterad tjänst. Här kan du till exempel länka till Havs- och vattenmyndighetens sajt med information om badplatser eller till kommunens egna APIer för vattenkvalitet.|
| 40 | visit\_url      | URL                             | Länk till allmän besöksinformation för platsen, t.ex. turistinformation eller liknande. Ange lämpligen den badplatssida på kommunens eller turistbolagets webbsida. Det här är den ingångssida som du önskar att besökare skall komma till för att lära sig mer om badmöjligheter i hela kommunen. Exempel på en sådan sida i Mariestads kommun: [Strandbad - Mariestads kommun](https://mariestad.se/Mariestads-kommun/Kultur--fritid/Bad/Strandbad.html)<br><br>Vi rekommenderar starkt att du använder detta attribut för att leda besökare till mer information kring bad i kommunen.|
| 41 | phone           | phone                            | Telefonnummer till huvudkontakt för platsen, t.ex. kommunens växel, turistinformation eller ägare av badplatsen.|
| 42 | email           | email                           | E-postadress för vidare kontakt, anges med gemener och med @ som avdelare. Använd ej mailto: eller andra HTML-koder. Exempel: info@mariestad.se<br><br>Epostadress ska anges enligt formatspecifikation i [RFC5322](https://datatracker.ietf.org/doc/html/rfc5322) i sektionen “addr-spec”. Verifiera att du följer denna.|

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.

### **decimaltal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas istället (då undviks problem med CSV formatet som använder komma som separator).

Den kanoniska representationen i xsd:decimal är påbjuden, dvs inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**
En URL är en webbadress enligt specifikationen RFC 1738. I detta sammanhang förutsätts schemana http eller https. För webbadresser med speciella tecken tillåts UTF-8-encodade domäner och pather, se då RFC 3986.

Observera att man inte får utelämna schemat, dvs "www.example.com" är inte en tillåten webbadress, däremot är "http://www.example.com" ok. Relativa webbadresser accepteras inte heller. (En fullständig regular expression utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att parsa, dvs “läsa in” din fil utan problem. Det finns flera onlineverktyg, t.ex. https://csvlint.io som du kan använda för att testa din fil.

### **boolean**
Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad “Boolesk” datatyp och kan antingen ha ett av två värden: Sant eller Falskt, men aldrig båda.

### **phone**
Reguljärt uttryck: **`/^\+?[\d-]{6,12}$/`**

För bästa internationella funktion rekommenderas att ange telefonnummer i internationellt format enligt [RFC3966](https://www.ietf.org/rfc/rfc3966). Då anger du landskod med + före riktnummer, utan inledande nolla på riktnummer. Telefonnummer till Töreboda kommuns växel, 0506-18000 anges internationell på följande sätt: `+46-506-18000`

Andra godtagbara och möjliga format, istället för internationellt nummer: `0506-180`

**Observera** att man inte får inkludera mellanslag i telefonummret då man vill kunna generera en webbadress (URI) utifrån numret vilket inte går om det är mellanslag i det.

## Förtydligande kring attributet place\_id
Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Observera att “place\_Id” inte är namnet på platsen utan dess ID. I fältet “name” skriver du in precis hur du vill att badplatsen ska representeras i text. “place\_id” är enbart till för att skapa en stabil identifiering som används när man ska låta en dator hantera informationen. Du kan jämför attributen “place\_id” och “name” som personnummer och namn för en människa. place\_id är personnummer och identifierar exakt en person. "name" är namn på personen och flera människor kan heta Ellen Ripley, men alla har ett unikt personnummer.Det är mycket viktigt att detta attribut är stabilt över tid. Det ska inte aldrig ändras. Om en badplats flyttas, stängs eller byter namn är det OK att skapa ett nytt place\_id men tillse att det gamla IDt slutar att existera och aldrig återanvänds.

Om du redan har en unik och stabil identifierare för din badplats i ett eget system, t.ex. ett GIS-system, ett anläggningsregister för fritidsområden, ett beskrivningsID för platsen i ett skötselsystem för en parkavdelning eller motsv, kan du använda denna istället som `place_id`. Du ansvarar för att namnet är unikt och stabilt över tid. Ett ID från ett verksamhetssystem inom kommunen duger gott, om du tillser att det är just unikt. Tre exempel på IDn från verksamhetssystem inom en fiktiv kommun:

* urn:ngsi-ld:Beach:se:gullspang:anlaggning:107
* toreboda.se-anlaggningsid-BC110-22001
* mariestad-badplats-Y021hattarevikensbadplats

Om du inte har en unik och stabil identifierare för din badplats redan, rekommenderas du att skapa en genom att ange en [kommunkod från SCB](https://www.scb.se/hitta-statistik/regional-statistik-och-kartor/regionala-indelningar/lan-och-kommuner/lan-och-kommuner-i-kodnummerordning/) för den kommun där badplatsen finns, följt av ett bindestreck “-” och namnet på badplatsen utan mellanslag och utan svenska tecken. Använd A-O och a-o, inga apostrofer, accenter, skiljetecken. Kommunkod anges alltid med fyra siffror med inledande nolla om sådan finns: <br>
`[kommunkod]-[NamnUtanSvenskaTeckenEllerMellanslag]`

Uttryckt som ett reguljärt uttryck blir detta: **`/^\d{4}-[a-zA-Z]+$/`**

Exempel 1 “Vristulven, Meybobadet” i Skövde kommun vid sjön Vristulven ger: 1496-VristulvenMeybobadet
Exempel 2 “Bjennsjöns Norra badplats” i Umeå kommun ger: 2480-BjennsjonsNorraBadplats

I Stockholm finns “Flatenbadet” men det finns två badplatser med samma namn. Du som vill publicera badplatser med lika namn måste alltså särskilja dem med ett unikt ID. Denna “krock” kan lösas genom att lägga till ett ord t.ex.:

* 0180-FlatenbadetBarnbadet
* 0180-FlatenbadetAllmanna

För badplats utanför Sverige som saknar kommunkod, anger du 9999. Exempel för badplats i Norge: 9999-StorforsevikaBadeplass

Unik och stabil identifierare för badet. Består förslagsvis av kommunkoden (4 siffror), ett bindestreck “-” och sedan namnet på badplatsen utan Å, Ä, Ö, bindestreck, mellanslag eller andra tecken som apostrof etc.. Endast bokstäver a-z och A-Z är tillåtet.

Exempel på icke tillåtna namn:

* 0180-Flatenbadet Norra
* 0180-FlatenbadetÖstra
* 0180-Flatenbadet Falénvägen 36
* Badplats 002 Gullspångs kommun
* Hattareviks Badplats Mariestad i Sverige
* 021 023 - Anläggning 12
* mariestad.se/bad/hattarevikens badplats Norra

Ovanstående namn korrigerade att bli giltiga:

* 0180-FlatenbadetNorra
* 0180-FlatenbadetOstra
* 0180-FlatenbadetFalenvagen
* Badplats002Gullspangskommun
* HattereviksBadplatsMariestadiSverige
* 021\_023\_Anlaggning\_12
* mariestad.se/bad/hattarevikens\_badplats\_norra
