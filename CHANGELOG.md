# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/sv/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Lägger till exempel på distribution i CSV, ODS och XLSX.

## [1.0.0] - 2021-06-21

### Added
- CSV-schema

### Changed
- Tydliggjort struktur med delvis annan kapitelindelning.
- Datatyper som kapitel istället för tabell.
- Flyttat förklarande texter från tabellen till datatypskapitlena.
- Tagit bort onödiga markeringar för frivilliga fält.
- Tagit bort duplicerad datatypsinformation från varje fält då det förklaras separat.
- Förtydligat och skrivit om delar av kapitlet om krav från dataportal.
- Markdown format för changelog.
- Lagt in länkar och versioner på Gitlab.

### Fixed
- Stavfel
- Flyttade in fotnot om LibreOffice och Excel i dokumentet.

## [0.9.2] - 2021-06-18

### Changed
- Förtydligat skillnaden mellan contributors och authors.
- Ändrat om strukturer och förtydligat kring datamodell.

## [0.9.1] - 2021-06-17
### Added
- README
- CHANGELOG
- Fullständig revisionshistorik finns nu tillgänglig på [CHANGELOG](https://gitlab.com/lankadedata/specifikationer/badplatser/-/blob/master/CHANGELOG)
- CODE OF CONDUCT

### Changed
- Informationsmodell och datatyper förtydligade
- Kvalitetsgranskning från dataportalens perspektiv
- Länkbar webbversion upplagd
- Översatt changelog från kollaborativt dokument till kodförråd i Markdown.

## [0.9.0] - 2021-06-16
### Added
- Lagt upp Markdown-mall som öppen källkod på [GitLab](https://gitlab.com/lankadedata/specifikationer/badplatser)

## [0.8.0] - 2021-06-02
### Changed
- Arrangerat om kapitlen och skapat appendix E, eventuellt stryker vi appendix E i slutgiltig spec och håller revisionshistoriken separat och inte i själva specifikationen, 
- Detsamma gäller Appendix D som bör hållas i ett annat levande dokument som vi kan fylla på allt eftersom nya ideer kommer fram.
- Överlämnat för publicering som öppen källkod till MetaSolutions.

## [0.7.0] - 2021-06-01
### Added
- Lagt till tabell för “förbättringar”.
- Lagt till tabell “Restlista”, appendix D. Fyllt på men en stor mängd “problem” som kan fixas till version 2.0 
- Första version av specifikationen. Vi går till publicering som version 1.0. Version 1.0 Release Candidate.
- En fullständig revisionshistorik kan begäras via Dataportal Väst 

### Changed
- Förtydligat ytterligare att man måste/ska underhålla datat i HoVs tjänst
- Bytt tillbaka namnet “nutscode” som ger ambivalens - det ÄR inte “NUTSCODE” utan ett påhittat ID som HoV skapat. Jag kallar nu attributet för “hov_ref” istället - då kan HoV byta namn på sitt attribut/ID utan att det påverkar den här specifikationen. 
- Ändrat några stavfel.
- Skrivit om hela sektionen kring NUTSKOD för att förtydliga och korta ner
- Förtydligat attribut 5 ytterligare
- Förtydligat toilet
- Förtydligat facilities
- Förtydligat 28, accessibility
- Rättat ett stort antal stavfel 
- Uppdaterat bakgrund, förkortat och tagit med perspektivet “utanför initiativet Dataportal Väst”.
- Uppdaterat förstasidan med lite småsaker

## [0.2.0] - 2021-05-31
### Changed
- Förtydligat licensformer för dokumentet (specifikationen, CC-BY-SA 4.0) och datat (CC0 1.0)
- Förtydligat RFC4180 som tillåter flera URL om man har behov, i fält som är för URL, dock bör man vara hygglig mot implementatören och separera dem på något trevlig sätt, t.ex. med CRLF och enligt RFC4180 med dubbla citationstecken. 
- Utökat förklaringen om place_id och rekommendationen är att använda det system som föreslås. Om man rdan har en egen identifierare kan man använda den istället, t.ex. om man redan delar data med specifikt ID och vill länka ihop datamängderna. Det viktiga är att IDt är unikt, stabilt över tid och inte återanvänds.

## [0.1.0] - 2021-05-28
### Added
- Skrivit hela sektionen om Tillgänglighetsdatabasen och dess API
- Lagt till contributor Magnus Sälgö
- Exempel för CSV och JSON skapat
- Innehållsförteckning
- Exempelfiler för Excel och CSV är skapade och taggade med exempeldata, datafilter/tips och instruktioner för export

### Changed
- Förtydligat kring licens för specifikation och hänvisar till sektion kring metadata för licens för själva datat. En förutsättning för att specen och datat ska bli fullt användbart skall datat släppas under licens CC0. 
- Förtydligat attribute “updated”, flyttat upp det till position 8 i filen
- Förtydligat ytterligare om NUTSKOD i sektionen.
- Numrerat om alla attribut igen
- Förtydligat decimalpunkt som standard
- Kontrollerat teckenkodning, vi kör på Unicode UTF-8
- Omnumrerat alla attribut igen, påbörjar exempelheader för CSV med komma och semikolonseparation. 
- Uppdaterat exempel för headers med och utan komma
- Fixat några stavfel
- Utökat “Observation 2” om datamodellen att rekommender användning av koordinater från kommunens GIS- och kartsystem om möjligt. Att samma koordinat finns i datamängden som i kommunens övriga system kommer att underlätta. Om GIS-systemet dessutom får besluta vara badplatsen ligger blir datat än mer korrekt och producenten av denna datamängd behöver inte fundera över eller riskera att skriva in fel koordinater. 
- Justerat text för att rymmas på en sida
- Justerat och utökat texten i sektion 2.1 för beskrivning av place_id

### Removed
- Strukit fältet “municipality” och förtydligat ytterligare “place_id” där kommunkoden skall ingå samt vad som gäller om man saknar kommunkod.
- Redigerat bort sidan som sa “Det här är ett utkast, ej klart”. 
- Åter strukit attribut description_en, döpt om description_se till desctiption och tillfört attribut “language_code”. 
- Strukit attribut “langugage_code” och istället ställt krav i kapitel 7 på att metadata som beskriver datats författade språk ska framgår vid publiceringen. Den som har behov av att dela data på annat språk uppmuntras göra det och i den distributionen sätta metadata till det språk som gäller. Det framgår i dokumentationen för DCAT-AP-SE hur man gör, dcterms:language används och beskrivs i kapitel 7 som ett krav på metadata som absolut skall ingå vid en publikation. 

## [0.0.9] - 2021-05-26
### Added
- Lagt till attribut description_eng och bytt namn på description till description_se
- Skrivit hela sektionen om Wikidata
- Skrivit hela sektionen om OpenStreetMap
- Lag tillbaka attribut 43 “updated” med datum i ISO8601 -format

### Changed
- Förtydligat fältet Wikidata
- Numrerat om listan
- Flyttat runt och skrivit om informationen om NUTSKOD på Havs- och vattenmyndigheten
- Ändrat lite typografi för läsbarhet, lagt till förklaring och regex för kommunkod, datum och place-ID. 
- Förtydligat ytterligare sektion place_id i kapitel 2.1
- Förtydligat om true/false och hur de ska användas i sektion 2.1
- Förtydligat attribut “beach_stone”
- Förtydligat attribut “beach_rock”

## [0.0.8] - 2021-05-25
### Added
- Lagt till fältet Kommunkod
- Beskrivit fältet place_id och förtydligat med flera exempel och valideringskod typ Regexp för att validera. 

## [0.0.7] - 2021-05-07
### Changed
- Förtydligat attributet Wheelchair - Kräver nu att hela badstranden är helt anpassad för bad med rullstol 

### Removed
- Strukit attribut “description_en” och förtydligat att du får skriva en översättning i description-fältet alternativt publicera datamängden en gång till på ytterligare språk lämpliga för området (ex. Engelska, Tyska, Franska, Italienska, Norsk/Dansk/Finländsk) etc.
- Strukit attributet “owner” som indikerade kommunal eller privat ägare - det blev svårt att tolka och någon bra identifierare för att peka ut ägare finns ej - bättre att man då anger tydliga kontaktuppgifter där ägar- och underhållsansvar får utredas/utvecklas. 

## [0.0.6] - 2021-05-06
### Added
- Lagt till fält “description_en” för beskrivning av fritext på engelska
- Lagt till attributet “drinking water”

### Changed
- Bytt namn på “ref_nuts” till “nutscode” på rekommendation
- Bytt namn på fält “description” till “description_se” för fritextbeskrivning på svenska
- Flyttat “place_id” till plats 1 igen
- Förtydligat attributet “toilet”

## [0.0.5] - 2021-05-04
### Changed
- Förtydligat om koordinatsystem med kapitelreferens till 2.1
- Namnändrar attribut “wc” som mer specificerar just “water closet” alltså toalett med spolning, ett gruppnamn för den typen av anläggning är bättre beskrivet som “toilet” som blir nya namnet. 
- Förtydligat datumformat sektion 2.1, strukit hänvisning till RFC3339 och typen “full date”.
- Förtydligat internationellt telefonnummerformat
- Attribut “water” är omdöpt till “watertype” för att särskilja och undvika konflikt med wikidatas attribut “water”. 
- Bytt namn på attribut “grilling_area” till “fireplace” och förklarat attributet.

### Removed
- Stryker attribut “amenity”, tidigare experiment för att länka till Wikidata
- Stryker attribut “ref_osm” då det inte finns beständiga identiteter i OpenStreetMap. Samtliga som föreslagit ändringar har rekommenderat Wikidata-referens istället.
- Stryker attribut “ref_googleplus” då det inte finns behov av flera system att peka ut en plats - vi kräver koordinat för varje objekt, varför ytterligare ett system inte behövs. Genom koordinat kan Pluskod härledas - alltså överflödigt attribut. 

## [0.0.4] - 2021-04-30
### Changed
- Bytt namn på nutskod
- Bytt namn övriga refrenser att börja med “ref_”
- Flyttat bort place_id som obligatorisk och lagt den som frivillig längre ned

### Removed
- Strukit attribut “public_transit” **
- Strukit attribut “public_transit_distance” **
- Strukit attribut “carp_parking” **
- Strukit attribut “car_parking_cost” **
- Strukit attribut “cycle_track” **
- ** Trafikdata/beskrivning fås från andra tjänster, t.ex. en karta eller en datamängd som beskriver trafik, infrastruktur och/eller parkeringsplatser.

## [0.0.3] - 2021-04-28
### Added
- Lagt till fält för “updated” som visar när raden senast uppdaterades/reviderades, lagt till förklaring över datatypen.
- Lagt till referens till Googles “pluskod” 
- Förslag/utkast till eget beständigt ID för varje rad “place_id”
 
## [0.0.2] - 2021-04-27
### Changed
- Omformulerat 1.3 observation kring engelska kolumnnamn. 
- Ändrat “city” till “postal_office” samt förtydligat “om finnes” då 5, 6, 7 är avhängigt om adress går att hitta.
- Omnumrerat alla attribut 1-43
- Förtydligat alla fälts beskrivning med orden “Frivilligt” samt motsvarande ord “Obligatoriskt” för obligatoriska fält
- 27, parking har döpts om till 28, car_parking med förtydligande att det är personbil som avses
- 28, parking_cost har döpts om till 29, car_parking_cost med förtydligande om att någon form av avgift gäller för den särskilt anordnade parkeringsplatsen?
- Ändrat definition av heltal och decimaltal så att “avsaknad av värde tolkas som ‘ej uppmätt’” från tidigare “tolkas som noll”. 
- Förtydligat kring NUTSKOD sektion 2.2
- Omformulerat punkt 40 “visit_url” 

### Removed
- Tagit bort begränsning textlängd på frivilliga fritextfält
- Sektion 3.1 struken “åtkomst via HTTP CSV”
- Sektion 4.1 struken “åtkomst via HTTP JSON”

### Fixed
- Stavfel/syftningsfel introduktion, en mening struken.
- Ändrat stavfel/översättning

## [0.0.1] - 2021-03-22

### Added
- Utkast till första version
- Utgår från specifikation för Leverantörsreskontra
- Första version för revision i samverkansgrupp

[Unreleased]: 
