
**Bakgrund**

Det dök upp behov av att inför sommarsäsongen 2021 presentera likvärdig information om regioners badplatser på att enkelt och likvärdigt sätt.

Specifikationen syftar till att ge deltagare i Dataportal Väst (och Sverige och internationellt) möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver badplatser på ett standardiserat och lika vis, samt länka samman informationen med Havs- och vattenmyndighetens data om badplatser och dess vattenkvalité, wikidata och övriga initiativ kring badvatten.

Utöver redaktörer och författare riktar vi ett stort tack till följande personer för deras bidrag:

**Isak Styf** - [DIGG](https://digg.se)<br>
**Josefin Lassinantti** - [DIGG](https://digg.se)<br>
**Felipe Verdú** - [Alingsås kommun](https://www.alingsas.se/)<br>
**Anders Andersson** - [UIT, Uppsala universitet](https://www.it.uu.se/)<br>
**Björn Hagström** - [Hagström Consulting AB](http://www.hagstrom.nu/)<br>
**Magnus Sälgö** - [OpenStreetMap Swedish mailing list](https://lists.openstreetmap.org/listinfo/talk-se)<br>
**Dennis P.** - [OpenStreetMap Swedish mailing list](https://lists.openstreetmap.org/listinfo/talk-se)<br>
**Erik Johansson** - [OpenStreetMap Swedish mailing list](https://lists.openstreetmap.org/listinfo/talk-se)<br>
**Ture Pålsson** - [OpenStreetMap Swedish mailing list](https://lists.openstreetmap.org/listinfo/talk-se)<br>

Vi har också haft stöd från följande organisationer:

**[Västra Götalandsregionen](https://www.vgregion.se/)** - Projektledning och inspiration.<br>
**[MetaSolutions](https://entryscape.com/en/)** - Tips, stöd och råd, samt teknisk bas för specifikantionshantering.<br>
**[ÖDIS](https://smartstad.stockholm/odis/)** - Ökad användning av öppna Data I Stockholmsregionen - idéer om utformning.
