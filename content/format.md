# Olika format
## CSV-formatet

Det enklaste formatet att stödja är CSV formatet enligt RFC4180. Se exemplet i Appendix A. Det som krävs är att första raden indikerar vilka kolumner som stöds:

<div class="example">
place_id,name,latitude,longitude,hov_ref,wikidata,updated,description,street,housenumber,postcode,city,country,facilities,toilet,shower,drinking_water,changing_room,lifeguard,lifebuoy,trash,firstaid,fireplace,bathing_jetty,bathing_ladder,diving_tower,td_url,accessibility,wheelchair,watertype,beach_sand,beach_stone,beach_rock,beach_concrete,beach_grass,pet_bath,camping,temp_url,extra_url,visit_url,phone,email
</div>

Utöver det som sägs i RFC4180 krävs alltid att informationen är uttryckt med teckenkodning UTF-8. På grund av att CSV har funnits länge och det finns en uppsjö av olika ramverk och program som inte alla följer RFC4180 så uppmanas man följa devisen:

<div class="note">
"Be conservative in what you do, be liberal in what you accept from others"
</div>

För den här specifikationen innebär det att implementatörer rekommenderas även stödja semikolonseparerade filer. Orsaken är att Microsoft Excel använder semikolon istället för komma i CSV filer på operativsystem med svenska som default. Detta på grund av att komma används för decimaltal vilket ofta förekommer i CSV-filer vilket skulle kräva en omfattande användning av dubbla citationstecken kring värden. Denna specifikation kräver dock att man använder punkt i decimaltal (se 2.1). Det finns alternativ, som t.ex. LibreOffice som ger användaren större kontroll över hur CSV filen uttrycks. Microsoft Excel är än så länge den vanligast förekommande i offentlig förvaltning i Sverige. 

För att särskilja data som är semikolon- från kommaseparerade måste första raden se ut som:

<div class="example">
place_id;name;latitude;longitude;hov_ref;wikidata;updated;description;street;housenumber;postcode;city;country;facilities;toilet;shower;drinking_water;changing_room;lifeguard;lifebuoy;trash;firstaid;fireplace;bathing_jetty;bathing_ladder;diving_tower;td_url;accessibility;wheelchair;watertype;beach_sand;beach_stone;beach_rock;beach_concrete;beach_grass;pet_bath;camping;temp_url;extra_url;visit_url;phone;email
</div>

Om värden saknas för en viss kolumn för en viss rad utelämnas värdet så att två komman (eller semikolon) kommer efter varandra. Att stoppa in nollvärden, t.ex. ett mellanslag, ett minus eller texten "null" är inte tillåtet och kommer att misstolkas.

### CSV-schema

Du som är nöjd med att leverera eller använda data badplatser utifrån vad som beskrivs skrifligen i denna specifikation och exemplifieras i appendixen kan ignorera detta avsnitt.

Sedan 2015 finns, "[Metadata Vocabulary for Tabular Data](https://www.w3.org/TR/tabular-metadata/)", en W3C rekommendation för hur man anger metadata för datamodeller uttryckta i CSV.

<div class="def">
**Schema för badplatser:** [https://lankadedata.se/spec/badplatser/schema.json](schema.json)
</div>

Syfte med schemat är att möjliggöra maskinell verifering av om CSV-uttryck följer denna specifikation.

## JSON formatet

JSON uttrycket följer direkt från CSV-schemat och W3C-rekommendationen "[Generating JSON from Tabular Data on the Web](https://www.w3.org/TR/csv2json/)". Se exempel på JSON i appendix B.