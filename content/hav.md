## Havs- och vattenmyndighetens e-tjänst

På Havs- och vattenmyndighetens webbplats för Badplatser [https://www.havochvatten.se/](https://www.havochvatten.se/) klicka dig fram till [https://www.havochvatten.se/badplatser-och-badvatten.html](Badplatser och badvatten) finns en webbtjänst i vilken du kan söka fram din respektive badplats per kommun. Här finns vissa uppgifter om din badplats (om den är publicerad) som till exempel koordinater. Verifiera att koordinaterna stämmer och återanvänd gärna samma koordinater.


### Publicera kommunens badplatser i Havs- och vattenmyndighetens tjänst!

Havs- och vattenmyndigheten rekommenderar dig att tipsa din kommun om:
*   **_Publicera alla bad, oavsett om de kvalitetsmäts eller ej_**, eller om de är EU-bad eller ej. Myndigheten vill att alla badplatser i hela Sverige är med i databasen!
*   Underhåll datat i HoVs databas/tjänst när du ändrar din datamängd
*   Underhåll din datamängd när du ändrar något i HoVs databas/tjänst
*   **_Uppdatera HoVs tjänst när du vill gå ut med avrådan från bad, t.ex. vid dåliga mätvärden, algblomningar eller annat. _**

En kontroll i maj 2021 visar att långt ifrån alla kommuner publicerar sina badvatten i Havs- och vattenmyndighetens tjänst. Uppmana din kommun att prioritera detta arbete!

### NUTSKOD

NUTSKOD är den beteckning för unikt ID som används och skapas av Havs- och vattenmyndigheten för av dem kända badplatser. Koden skapas när ett nytt bad läggs upp i deras tjänst. En badplats som inte finns i myndighetens datakälla, har ingen fastställd NUTSKOD.

NUTSKOD baseras delvis på det NUTS-system som fastställts av Eurostat/EU **_men är inte samma sak - OBS!_**

NUTSKOD visas inte i webbtjänsten på Havs- och vattenmyndighetens webbplats, utan nås via ett API. För att förenkla har vi skapat ett kalkylblad i Excel som konsumerar APIet från myndigheten och visa kommunnamn, badplatsnamn, NUTSKOD samt latitud och longitud.

Du kan kopiera och klistra in NUTSKOD från denna excel-fil (eller slå upp din badplats själv i APIet). Värdet i NUTSKOD ska anges på din datamängd på attribut 5, “hov_ref”. Saknas din badplats kan du lämna “hov_ref” tomt. Helst önskar vi att du lägger upp badplatsen i Havs- och vattenmyndighetens tjänst och tar den NUTSKOD som skapas och lägger till i din data.

### API/webbtjänst hos Havs- och vattenmyndigheten

Om du själv önskar nå Havs- och vattenmyndighetens API, har vi använt följande åtkomstpunkt för datat: [https://badplatsen.havochvatten.se/badplatsen/api/feature/](https://badplatsen.havochvatten.se/badplatsen/api/feature/). Dokumentation saknas i skrivande stund på myndighetens webbplats men kan fås vid begäran direkt från myndigheten. Enligt uppgift (maj 2021) pågår ett arbete med att förfina APIer och dokumentation på webbplatsen. I skrivande stund kan man länka direkt till Havs- och vattenmyndighetens karta över badplatsen genom följande länk, som skulle kunna användas i attribut 39, “extra_url”: [https://badplatsen.havochvatten.se/badplatsen/karta/#/bath/[NUTSKOD]](https://badplatsen.havochvatten.se/badplatsen/karta/#/bath/[NUTSKOD).

Innan du publicerar bör du prova så att länken fungerar. Ersätt [NUTSKOD] i länken ovan med kod till din specifika badplats. 
